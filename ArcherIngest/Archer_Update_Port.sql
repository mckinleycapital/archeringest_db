USE [PortfolioMGMT]
GO

/****** Object:  StoredProcedure [dbo].[Archer_Update_Port]    Script Date: 9/20/2017 7:48:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		David Burdick
-- Create date: 09/14/2017
-- Description:	Stored Procedure to update the Port table with data from Archer for APL accounts
-- =============================================
CREATE PROCEDURE [dbo].[Archer_Update_Port]
	-- Add the parameters for the stored procedure here
	--<@Param1, sysname, @p1> <Datatype_For_Param1, , int> = <Default_Value_For_Param1, , 0>, 
	--<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	/*** This inserts the accounts from the ArcherHoldings table into the port table 
     using information from the portfolios table. ***/


	DELETE FROM Port WHERE FILENAME IS NULL
	
	INSERT INTO Port (Client_ID, Name, Account, Brok, PGroupID, LastDate, LastTime)
	SELECT DISTINCT P.ClientID, LEFT(P.Name,40), P.AcctNumber, P.BrokerCode, P.PgroupID, CONVERT(SMALLDATETIME,CONVERT(CHAR,GETDATE(),101)) AS LastDate, CONVERT(NVARCHAR(10),GETDATE(),108) AS LastTime
	FROM         [dbo].[ArcherHoldings] AH inner join Portfolios P on AH.[Cust Acct ID] = replace(AcctNumber,'-', '')
	WHERE     (P.Source <> 1)	
	AND P.CloseDate is NULL

	/* This updates the missing fields in the APL accounts in the Port Table */

	UPDATE PORT
	SET MRKTVAL = Tot_MV.MRKTVAL,
	NETCASH = Tot_Cash.NetCash,
	PCTASSETS = (Tot_Cash.NetCash/Tot_MV.MRKTVAL)*100
	FROM   	(SELECT     CLIENT_ID, SUM(MARKET_VAL) AS MRKTVAL
		 FROM         ARCHER_PORTDET
		 GROUP BY CLIENT_ID) Tot_MV,
		(SELECT     CLIENT_ID, MARKET_VAL AS netcash
		 FROM         ARCHER_PORTDET
		 WHERE     (TICKER = 'cash')) Tot_Cash, PORT
	WHERE PORT.Client_ID = Tot_MV.Client_ID
	AND PORT.Client_ID = Tot_Cash.Client_ID
	AND PORT.Client_ID IN (SELECT ClientID FROM Portfolios WHERE source <> 1)

	/* This updates the Percent of assets and Percet Gain fields in PortDet */
UPDATE ARCHER_PORTDET
	SET PC_ASSETS = (MARKET_VAL/P.MRKTVAL)*100 
FROM PORT P, ARCHER_PORTDET AP
WHERE P.CLIENT_ID = AP.CLIENT_ID
AND P.CLIENT_ID IN (SELECT ClientID FROM Portfolios WHERE Source <> 1)

UPDATE ARCHER_PORTDET
	SET PC_GL = ((MARKET_VAL - TOT_COST) / TOT_COST) * 100 
FROM PORT P, ARCHER_PORTDET AP
WHERE P.CLIENT_ID = AP.CLIENT_ID
AND TOT_COST <> 0
AND P.CLIENT_ID IN (SELECT ClientID FROM Portfolios WHERE Source <> 1)

END

GO


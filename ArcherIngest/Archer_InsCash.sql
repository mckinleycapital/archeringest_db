USE [PortfolioMGMT]
GO

/****** Object:  StoredProcedure [dbo].[Archer_InsCash]    Script Date: 9/20/2017 7:47:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		David Burdick
-- Create date: 09/15/2017
-- Description:	Insert the cash values into the Archer_Portdet table from the ArcherHoldings table.
-- =============================================
CREATE PROCEDURE [dbo].[Archer_InsCash] 
	-- Add the parameters for the stored procedure here
	--<@Param1, sysname, @p1> <Datatype_For_Param1, , int> = <Default_Value_For_Param1, , 0>, 
	--<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO ARCHER_PORTDET
	SELECT P.clientid AS CLIENT_ID
		,P.PgroupID 
		,P.PgroupID + ' ' + 'CASH' AS PGRPTICKER
		,'causn' AS TYPE
		,CONVERT(decimal(18,4),AH.Quantity) AS QUANTITY 
		,'CASH' AS TICKER
		,left(AH.[Cusip / Sedol],8) AS CUSIP
		, CONVERT(decimal(18,4),ah.[Unit Cost Base]) AS UNIT_COST
		, CONVERT(decimal(18,4),ah.[Total Cost Base]) AS TOT_COST
		, CONVERT(decimal(18,4),ah.[Market Price]) AS PRICE
		, CONVERT(decimal(18,4), ah.[Market Value Local]) AS MARKET_VAL
		, NULL
		, NULL
	FROM [dbo].[ArcherHoldings] AH INNER JOIN Portfolios P ON AH.[Cust Acct ID] = REPLACE(AcctNumber,'-', '') 
	WHERE ah.Symbol = 'usd' AND ah.[Cusip / Sedol] = 'CASHUSD'
	AND P.Closedate IS NULL
	ORDER BY p.clientid, ah.symbol
END

GO


USE [PortfolioMGMT]
GO

/****** Object:  StoredProcedure [dbo].[Archer_InsStock]    Script Date: 9/20/2017 7:47:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		David Burdick
-- Create date: 09/15/2017
-- Description:	Insert the stock positions into the Archer_Portdet table.
-- =============================================
CREATE PROCEDURE [dbo].[Archer_InsStock] 
	-- Add the parameters for the stored procedure here
	--<@Param1, sysname, @p1> <Datatype_For_Param1, , int> = <Default_Value_For_Param1, , 0>, 
	--<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO ARCHER_PORTDET
	SELECT P.clientid AS CLIENT_ID
		,P.PgroupID 
		,P.PgroupID + ' ' + S.symbol AS PGRPTICKER
		,s.type + 'n' AS TYPE
		,CONVERT(DECIMAL(18,4),AH.Quantity) as QUANTITY 
		,s.symbol AS TICKER
		,LEFT(AH.[Cusip / Sedol],8) AS CUSIP
		, CONVERT(DECIMAL(18,4),ah.[Unit Cost Base]) AS UNIT_COST
		, CONVERT(DECIMAL(18,4),ah.[Total Cost Base]) AS TOT_COST
		, CONVERT(DECIMAL(18,4),ah.[Market Price]) AS PRICE
		, CONVERT(DECIMAL(18,4), ah.[Market Value Local]) AS MARKET_VAL
		, NULL
		, NULL
	FROM [dbo].[ArcherHoldings] AH INNER JOIN Portfolios P ON AH.[Cust Acct ID] = REPLACE(AcctNumber,'-', '') 
		left join secinf s on AH.[cusip / Sedol] = s.cusip AND AH.[Symbol] = REPLACE(s.symbol, '/', ' ')
	WHERE s.type in ('csus', 'feus','caus','mfus')
	AND p.closedate IS Null
	ORDER BY p.clientid, ah.symbol
END

GO

